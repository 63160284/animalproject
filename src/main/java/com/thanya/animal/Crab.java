/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.animal;

/**
 *
 * @author Thanya
 */
public class Crab extends AquaticAnimal {

    private String nickname;

    public Crab(String nickname) {
        super("Crab", 10);
        this.nickname = nickname;
    }

    @Override
    public void swim() {
        System.out.println("Crab: " + nickname + " swim");
    }

    @Override
    public void eat() {
        System.out.println("Crab: " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Crab: " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Crab: " + nickname + " can't speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crab: " + nickname + " sleep");
    }

}
