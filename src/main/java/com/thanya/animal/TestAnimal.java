/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.animal;

/**
 *
 * @author Thanya
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("Khao");
        h1.run();
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        System.out.println("h1 is animal ?" + (h1 instanceof Animal));
        System.out.println("h1 is land animal ?" + (h1 instanceof LandAnimal));
        Animal a1 = h1;
        System.out.println("a1 is land animal ?" + (a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal ?" + (a1 instanceof Reptile));
        System.out.println("a1 is aquatic animal ?" + (a1 instanceof AquaticAnimal));
        System.out.println("a1 is poultry animal ?" + (a1 instanceof Poultry));

        Cat c1 = new Cat("Bubu");
        c1.run();
        c1.eat();
        c1.walk();
        c1.speak();
        c1.sleep();
        System.out.println("c1 is animal ?" + (c1 instanceof Animal));
        System.out.println("c1 is land animal ?" + (c1 instanceof LandAnimal));
        Animal a2 = c1;
        System.out.println("a2 is land animal ?" + (a1 instanceof LandAnimal));
        System.out.println("a2 is reptile animal ?" + (a1 instanceof Reptile));
        System.out.println("a2 is aquatic animal ?" + (a1 instanceof AquaticAnimal));
        System.out.println("a2 is poultry animal ?" + (a1 instanceof Poultry));

        Dog d1 = new Dog("Dook");
        d1.run();
        d1.eat();
        d1.walk();
        d1.speak();
        d1.sleep();
        System.out.println("d1 is animal ?" + (d1 instanceof Animal));
        System.out.println("d1 is land animal ?" + (d1 instanceof LandAnimal));
        Animal a3 = d1;
        System.out.println("a3 is land animal ?" + (a3 instanceof LandAnimal));
        System.out.println("a3 is reptile animal ?" + (a3 instanceof Reptile));
        System.out.println("a3 is aquatic animal ?" + (a3 instanceof AquaticAnimal));
        System.out.println("a3 is poultry animal ?" + (a3 instanceof Poultry));

        Crocodile croc = new Crocodile("Bob");
        croc.crawl();
        croc.eat();
        croc.walk();
        croc.speak();
        croc.sleep();
        System.out.println("croc is animal ?" + (croc instanceof Animal));
        System.out.println("croc is reptile animal ?" + (croc instanceof Reptile));
        Animal a4 = croc;
        System.out.println("a4 is land animal ?" + (a4 instanceof LandAnimal));
        System.out.println("a4 is reptile animal ?" + (a4 instanceof Reptile));
        System.out.println("a4 is aquatic animal ?" + (a4 instanceof AquaticAnimal));
        System.out.println("a4 is poultry animal ?" + (a4 instanceof Poultry));
        
        Snake s = new Snake("Poppy");
        s.crawl();
        s.eat();
        s.walk();
        s.speak();
        s.sleep();
        System.out.println("s is animal ?" + (s instanceof Animal));
        System.out.println("s is reptile animal ?" + (s instanceof Reptile));
        Animal a5 = s;
        System.out.println("a5 is land animal ?" + (a5 instanceof LandAnimal));
        System.out.println("a5 is reptile animal ?" + (a5 instanceof Reptile));
        System.out.println("a5 is aquatic animal ?" + (a5 instanceof AquaticAnimal));
        System.out.println("a5 is poultry animal ?" + (a5 instanceof Poultry));
        
        Fish f = new Fish("Nemo");
        f.swim();
        f.eat();
        f.walk();
        f.speak();
        f.sleep();
        System.out.println("f is animal ?" + (f instanceof Animal));
        System.out.println("f is aquatic animal ?" + (f instanceof AquaticAnimal));
        Animal a6 = f;
        System.out.println("a6 is land animal ?" + (a6 instanceof LandAnimal));
        System.out.println("a6 is reptile animal ?" + (a6 instanceof Reptile));
        System.out.println("a6 is aquatic animal ?" + (a6 instanceof AquaticAnimal));
        System.out.println("a6 is poultry animal ?" + (a6 instanceof Poultry));
        
        Crab crab = new Crab("Mil");
        crab.swim();
        crab.eat();
        crab.walk();
        crab.speak();
        crab.sleep();
        System.out.println("crab is animal ?" + (crab instanceof Animal));
        System.out.println("crab is aquatic animal ?" + (crab instanceof AquaticAnimal));
        Animal a7 = crab;
        System.out.println("a7 is land animal ?" + (a7 instanceof LandAnimal));
        System.out.println("a7 is reptile animal ?" + (a7 instanceof Reptile));
        System.out.println("a7 is aquatic animal ?" + (a7 instanceof AquaticAnimal));
        System.out.println("a7 is poultry animal ?" + (a7 instanceof Poultry));
        
        Bat b = new Bat("sun");
        b.fly();
        b.eat();
        b.walk();
        b.speak();
        b.sleep();
        System.out.println("b is animal ?" + (b instanceof Animal));
        System.out.println("b is poultry animal ?" + (b instanceof Poultry));
        Animal a8 = b;
        System.out.println("a8 is land animal ?" + (a8 instanceof LandAnimal));
        System.out.println("a8 is reptile animal ?" + (a8 instanceof Reptile));
        System.out.println("a8 is aquatic animal ?" + (a8 instanceof AquaticAnimal));
        System.out.println("a8 is poultry animal ?" + (a8 instanceof Poultry));
        
        Bird bi = new Bird("Pao");
        bi.fly();
        bi.eat();
        bi.walk();
        bi.speak();
        bi.sleep();
        System.out.println("bi is animal ?" + (bi instanceof Animal));
        System.out.println("bi is poultry animal ?" + (bi instanceof Poultry));
        Animal a9 = bi;
        System.out.println("a9 is land animal ?" + (a9 instanceof LandAnimal));
        System.out.println("a9 is reptile animal ?" + (a9 instanceof Reptile));
        System.out.println("a9 is aquatic animal ?" + (a9 instanceof AquaticAnimal));
        System.out.println("a9 is poultry animal ?" + (a9 instanceof Poultry));
    }
}
